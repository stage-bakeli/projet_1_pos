import ApplicationLogo from '@/Components/ApplicationLogo';
import { Link } from '@inertiajs/react';
import photo from '../../../public/assets/images/logo.png'
export default function Guest({ children }) {
    return (
        <div className="min-h-screen flex flex-col sm:justify-center items-center py-6 sm:pt-0 bg-gray-100">

            <div className="w-full sm:max-w-md mt-6 bg-white shadow-md overflow-hidden sm:rounded-lg">

                <div className="formLogo d-flex  justify-content-center align-items-center ">
                    <img src={photo} className='imgLogo' />
                </div>
                <div className='p-4'>

                    {children}
                </div>

            </div>
        </div>
    );
}
