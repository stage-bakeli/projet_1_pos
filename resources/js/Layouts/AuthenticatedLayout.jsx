import { useState } from 'react';
import ApplicationLogo from '@/Components/ApplicationLogo';
import Dropdown from '@/Components/Dropdown';
import NavLink from '@/Components/NavLink';
import ResponsiveNavLink from '@/Components/ResponsiveNavLink';
import { Link } from '@inertiajs/react';
import NavBar from '@/Components/NavBar';
import SideBar from '@/Components/SideBar';

export default function Authenticated({ user, header, children }) {
    const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);


    return (
        <div>
            <div className="layout-nav-side position-relative ">
                <div className="d-flex ">
                    <SideBar />
                    <div className="lNS-droit p-0">
                        <NavBar user={user} />
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
}
