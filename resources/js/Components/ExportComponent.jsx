export default function getLocStorage(setData) {
	const storedProduits = localStorage.getItem("produits");
	let produits = JSON.parse(storedProduits);
	if (produits === null) {
	  produits = [];
	}
	setData(produits);
  
	return produits;
  }
  
  