import React, { useEffect, useState } from 'react'
import InputLabel from '../InputLabel'
import TextInput from '../TextInput'
import InputError from '../InputError'
import PrimaryButton from '../PrimaryButton'
import { useForm } from '@inertiajs/react'
import Swal from 'sweetalert2'
import axios from 'axios'

export default function FormCateg({ name, user_id }) {
	const [isValid, setIsValid] = useState(false);
	const [nomError, setNomError] = useState('');
	const [descError, setDescError] = useState('');
	const [emailError, setEmailError] = useState('');
	const [telephoneError, setTelephoneError] = useState('');
	const [adresseError, setAdresseError] = useState('');





	const url = `http://127.0.0.1:8000/api/${name}`;

	const { data, setData, post, processing, errors, reset } = useForm();

	// useEffet de regEx
	useEffect(() => {
		if (name === 'categories') {
			const regexNom = /^.{5,30}$/;
			const isNomValid = regexNom.test(data.nom);
			setNomError(isNomValid ? '' : 'Le nom doit avoir entre 5 et 30 caractères');

			const regexDesc = /^.{10,500}$/;
			const isDescValid = regexDesc.test(data.description);
			setDescError(isDescValid ? '' : 'Faites une description de la catégorie d\' au moin 10 caratères');

			setIsValid(isNomValid && isDescValid);
		} else {
			const regexNom = /^.{5,30}$/;
			const isNomValid = regexNom.test(data.nom);
			setNomError(isNomValid ? '' : 'Le nom doit avoir entre 5 et 30 caractères');

			// Validation des champs
			const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
			const isEmailValid = regexEmail.test(data.email);
			setEmailError(isEmailValid ? '' : 'L\'adresse email n\'est pas valide');


			const regexTelephone = /^\d{9}$/;
			const isTelephoneValid = regexTelephone.test(data.telephone);
			setTelephoneError(isTelephoneValid ? '' : 'Le numéro de téléphone doit comporter 9 chiffres');

			const regexAdresse = /^.{5,30}$/;
			const isAdresseValid = regexAdresse.test(data.adresse);
			setAdresseError(isAdresseValid ? '' : 'L\'adresse doit avoir entre 5 et 30 caractères');

			setIsValid(isEmailValid && isAdresseValid && isTelephoneValid && isNomValid);
		}

	}, [data]);

	const submit = async (e) => {
		e.preventDefault();

		if (isValid === true) {

			if (name === 'categories') {
				let { nom, description } = data;

				const donnees = {
					user_id: user_id,
					nom: nom,
					description: description,
				}
				await axios.post(url, donnees).then(({ data }) => {
					Swal.fire({
						icon: "success",
						text: data.message
					})
					window.location.replace(`http://127.0.0.1:8000/donnees/${name}`)
					reset();
				}).catch(({ response }) => {
					if (response?.status === 422) {
						// setValidationError(response.data.errors)
						console.log(response.data.errors)
					} else {
						Swal.fire({
							text: response.data.message,
							icon: "error"
						})
					}
				})
			} else {
				let { nom, email, telephone, adresse, } = data;

				const donnees = {
					user_id: user_id,
					nom: nom,
					email: email,
					telephone: telephone,
					adresse: adresse
				}
				await axios.post(url, donnees).then(({ data }) => {
					Swal.fire({
						icon: "success",
						text: data.message
					})
					window.location.replace(`http://127.0.0.1:8000/donnees/${name}`)
					reset();
				}).catch(({ response }) => {
					if (response?.status === 422) {
						// setValidationError(response.data.errors)
						console.log(response.data.errors)
					} else {
						Swal.fire({
							text: response.data.message,
							icon: "error"
						})
					}
				})
			}
		}


	}

	return (
		<form onSubmit={submit} encType="multipart/form-data">
			{name === 'categories' ? (<>  <div>
				<InputLabel htmlFor='nom' value='Nom' />
				<TextInput
					id="nom"
					name="nom"
					value={data?.nom}
					className="mt-1 block w-full"
					autoComplete="nom"
					isFocused={true}
					onChange={(e) => setData('nom', e.target.value)}
					ref={register}
				/>
				{nomError && <p className='text-danger'>{nomError}</p>}
			</div>
				<div>
					<InputLabel htmlFor='description' value='Description' />
					<textarea
						id="description"
						name="description"
						value={data?.description}
						className="mt-1 block w-full"
						autoComplete="description"
						onChange={(e) => setData('description', e.target.value)}
						ref={register}
					/>
					{descError && <p className='text-danger'>{descError}</p>}
				</div>
			</>) : (<>
				<div>
					<InputLabel htmlFor='nom' value='Nom' />
					<TextInput
						id="nom"
						name="nom"
						value={data?.nom}
						className="mt-1 block w-full"
						autoComplete="nom"
						onChange={(e) => setData('nom', e.target.value)}

					/>
					{nomError && <p className='text-danger'>{nomError}</p>}
				</div>
				<div>
					<InputLabel htmlFor='email' value='Email' />
					<TextInput
						id="email"
						name="email"
						type='email'
						value={data?.email}
						className="mt-1 block w-full"
						autoComplete="email"
						onChange={(e) => setData('email', e.target.value)}

					/>




					{emailError && <p className='text-danger'>{emailError}</p>}
				</div>
				<div>
					<InputLabel htmlFor='telephone' value='Telephone' />
					<TextInput
						id="telephone"
						name="telephone"
						value={data?.telephone}
						type='phone'
						className="mt-1 block w-full"
						autoComplete="telephone"
						onChange={(e) => setData('telephone', e.target.value)}

					/>

					{telephoneError && <p className='text-danger'>{telephoneError}</p>}
				</div>
				<div>
					<InputLabel htmlFor='adresse' value='Adresse' />
					<TextInput
						id="adresse"
						name="adresse"
						value={data?.adresse}
						className="mt-1 block w-full"
						autoComplete="adresse"
						onChange={(e) => setData('adresse', e.target.value)}

					/>

					{adresseError && <p className='text-danger'>{adresseError}</p>}
				</div>
			</>)}

			<div className='sm:flex justify-center py-2.5'>
				<PrimaryButton className="ml-4 py-2.5 text-uppercase" disabled={processing}>
					Ajout {name}
				</PrimaryButton>
			</div>
		</form>
	)
}
