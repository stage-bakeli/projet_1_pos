import React from 'react'
import { FcPlus } from 'react-icons/fc';
import Swal from 'sweetalert2';
import getLocStorage from '../ExportComponent';
import { MdDeleteForever } from 'react-icons/md';
import { GrSubtractCircle } from 'react-icons/gr';




export default function ListePanier({ data, setData }) {

	
	const DeleteProduct = (id_product) => {

		const produits = JSON.parse(localStorage.getItem('produits'));

		const nouveauxProduits = produits.filter((produit) => produit.id !== id_product);

		localStorage.setItem('produits', JSON.stringify(nouveauxProduits));
		getLocStorage(setData);


	}

	const subProduct =(id_product)=>{
		const produits = JSON.parse(localStorage.getItem('produits'));

		const differentProduits = produits.filter((produit) => produit.id !== id_product);
		const memeIdProduits = produits.filter((produit) => produit.id == id_product);
				
		if (memeIdProduits.length>1) {
			memeIdProduits.shift();
			
			const newProduct = [].concat(differentProduits,memeIdProduits)
			localStorage.setItem("produits", JSON.stringify(newProduct));
		}else{
			localStorage.setItem("produits", JSON.stringify(differentProduits));
		}
		
		getLocStorage(setData);

	}

	const addlocalStrage = (datas) => {
		const storedProduits = localStorage.getItem("produits");
		const produits = JSON.parse(storedProduits) || [];
		produits.push({
			id: datas.data.id,
			nom: datas.data.nom,
			image: datas.data.image,
			prix: datas.data.prix,
			categorie: datas.data.categorie,
		});
		localStorage.setItem("produits", JSON.stringify(produits));
		getLocStorage(setData);
	};
	return (
		<div className='py-2 listepanier'>
			<ul className="list-group  d-flex flex-column gap-2">
				{Object.values(data)?.map((element, index) => {
					return <li className="list-group-item d-flex justify-content-between align-items-start" key={index}>
						<div className="">
							<img src={`produit/${element?.data?.image}`} alt="" className='img-fluid' />
						</div>
						<div>
							<div><strong>{element?.data?.nom}</strong></div>
							<div>
								<span className="badge bg-secondary rounded-pill">Prix unitaire :{element?.data?.prix}f</span>
							</div>
							<div>
								<span className="badge bg-info rounded-pill">Prix total :{element?.data?.prix * element?.occurrences}f</span>
							</div>
							<div className='d-flex'>
								<div>
									<span className="badge bg-primary rounded-pill">{element?.occurrences}</span>
								</div>
								<div>
									<button onClick={() => { addlocalStrage(element) }}><FcPlus className="fs-3" /></button>
								</div>
								<div>
									<button onClick={() => { subProduct(element?.data?.id) }}><GrSubtractCircle className="fs-4 text-warning" /></button>
								</div>
								<div>
									<button onClick={() => { DeleteProduct(element?.data?.id) }}><MdDeleteForever className="fs-3 text-danger" /></button>
								</div>
							</div>
						</div>
					</li>
				})}

			</ul>
		</div>
	)
}
