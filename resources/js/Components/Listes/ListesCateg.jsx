import { InertiaLink } from '@inertiajs/inertia-react';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import fetchData from '../fecthData';

export default function ListesCateg({ name }) {


	const [products, setProducts] = useState([])

	useEffect(() => {
		fetchData(setProducts, name)
	}, [])

	const deleteProduct = async (id) => {
		const isConfirm = await Swal.fire({
			title: 'Etes-vous sûr(e) ?',
			text: "Vous ne pourrez pas revenir en arrière!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: 'Annuler',
			confirmButtonText: 'Oui, supprimez-le!'
		}).then((result) => {
			return result.isConfirmed
		});

		if (!isConfirm) {
			return;
		}

		await axios.delete(`http://127.0.0.1:8000/api/${name}/${id}`).then(({ data }) => {
			Swal.fire({
				icon: "success",
				text: data.message,
				timer: 2000,
				timerProgressBar: true,
				showConfirmButton: false,
			})
			fetchData(setProducts, name);
		}).catch(({ response: { data } }) => {
			Swal.fire({
				text: data.message,
				icon: "error"
			})
		})

	}


	return (
		<div>
			<table className="" id="customers">
				<thead>
					<th>N°</th>
					<th>Nom</th>
					<th>Description</th>
					<th>Action</th>
				</thead>
				<tbody>
					{products?.map((item, index) => {
						return <tr key={index}>
							<td>{index + 1}</td>
							<td>{item?.nom}</td>
							<td>{item?.description}</td>
							<td className='d-flex'>
								<button className='btn  btn-info'>
									<InertiaLink href={`/donnees/${name}/${item?.id}/show`}>
										<span className="icon-bg"><i className="mdi mdi-eye menu-icon"></i></span>
										<span className="menu-title">Detail</span>
									</InertiaLink>
								</button>
								<button className='btn mx-2 btn-primary'>
									<InertiaLink href={`/donnees/${name}/${item?.id}/edit`}>
										<span className="icon-bg"><i className="mdi mdi-lead-pencil menu-icon"></i></span>
										<span className="menu-title">Editer</span>
									</InertiaLink>
								</button>
								<button className='btn  btn-danger' onClick={() => deleteProduct(item?.id)}>
									<span className="icon-bg"><i className="mdi mdi-delete menu-icon"></i></span>
									supprimé</button>
							</td>
						</tr>
					})}
				</tbody>
			</table>
		</div>
	)
}
