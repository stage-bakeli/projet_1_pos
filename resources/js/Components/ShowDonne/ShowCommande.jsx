import React, { useEffect, useState } from 'react'
import { MdOutlineEmail, MdOutlinePhonelinkRing, MdLocationOn, MdPrint } from 'react-icons/md'

export default function ShowCommande({ id, name, }) {


	const [donnees, setDonnees] = useState();
	const [client, setClient] = useState();

	const fetchFacture = async () => {
		await axios.get(`http://localhost:8000/api/${name}/${id}`).then(({ data }) => {
			setDonnees(data.data);
			fethClient(data.data.id_client);
		});
	};

	const fethClient = async (clientId) => {
		await axios.get(`http://localhost:8000/api/clients/${clientId}`).then(({ data }) => {
			setClient(data.data);
		});
	};

	useEffect(() => {
		fetchFacture();
	}, []);


	if (!donnees) {
		// Si les données de facture ne sont pas encore récupérées, vous pouvez afficher un message de chargement ou un spinner ici
		return <div>Chargement...</div>;
	}

	// Le reste de votre code




	let my_liste = donnees?.my_liste;


	const handlePrint = () => {
		window.print();
	}



	return (
		<div className='container p-4'>
			<div className='row monfacture'>

				<div className='col-md-8 colone_a_imprimer p-4'>
					<h4 className='fs-3 fw-bold text-center'>Facture</h4>
					<div className='d-flex justify-content-between'>
						<div>
							<div className='d-flex justify-content-center'>
								<img src={`http://127.0.0.1:8000/assets/images/logo.png`} alt="" width={100} />
							</div>
							<div className='d-flex gap-2'>
								<span><MdLocationOn /></span>
								<span>HLM Grand Yoff</span>
							</div>
							<div className='d-flex gap-2'>
								<span>
									<MdOutlineEmail />
								</span>
								<span>
									pos.fewnu@gmail.com
								</span>
							</div>
							<div className='d-flex gap-2'>
								<span>
									<MdOutlinePhonelinkRing />
								</span>
								<span>+221 77-777-77-77</span>
							</div>
						</div>
						<div>
							<div className='py-4 fw-bold'>Date :{donnees?.updated_at}</div>
							<div className='client'>
								<h3 className='text-center fs-4 fw-bold'>Client</h3>
								{client === undefined ? <p className='fs-6 fw-bold text-center'>Aucun client selectionné</p> : <>

									<div className='d-flex gap-2'>
										<span>Nom:</span>
										<span className='fs-6 fw-bold'>{client?.nom}</span>
									</div>
									<div className='d-flex gap-2'>
										<span>Numero:</span>
										<span className='fs-6 fw-bold'>{client?.telephone}</span>
									</div>
								</>}
							</div>
						</div>
					</div>
					<table class="table table-striped .table-bordered mt-2">
						<thead>
							<tr>
								<th> Produit </th>
								<th>  Quantité </th>
								<th> Prix unitaire </th>
								<th> Prix total </th>
							</tr>
						</thead>
						<tbody>{my_liste?.map(function (item, index) {
							return <tr key={index}>
								<td>{item.nom}</td>
								<td>{item.occurrences}</td>
								<td>{item.prix}</td>
								<td>{item.prix * item.occurrences}</td>
							</tr>
						})}
						</tbody>

					</table>

					<div className="d-flex justify-content-end pt-3">
						<div className="d-flex gap-1 justify-content-end">
							<h3 className='fs-6 fw-bold'>Prix total {donnees?.total}f </h3>
							<span className='fs-6 fw-bold'>{` (${donnees?.nombreArticle} articles)`}</span>
						</div>
					</div>
				</div>
				<div className='col-md-4 px-3'>
					<div className="factAction py-3">
						<div className=' d-flex justify-content-center gap-2 align-items-center fw-bold '>
							<p className='fs-4'>Status :</p>
							<p>{donnees?.payer === false ? <span className='text-danger fs-4'>Non payer</span> : <span className='text-success fs-4'>Déja payer</span>}</p>
						</div>
						<div className=' d-flex justify-content-evenly mt-5'>
							<button className='btn btn-secondary d-flex' onClick={() => handlePrint()}> <MdPrint /> Imprimer</button>
							<button className='btn btn-info '>Modifier</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}
