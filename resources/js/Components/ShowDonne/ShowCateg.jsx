import React, { useEffect, useState } from 'react'

export default function ShowCateg({ datas, name, donnees,fornisseur }) {



	// useEffect(() => { fetchProducts() }, [])
	return (
		<>
			{/* Categorie */}
			{name === "categories" ? <div >
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Nom:</p>
					<p className='fs-5 fw-bold'>{datas?.nom}</p>
				</div>
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Description:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.description}</span>
				</div>
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Date d'Ajout:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.updated_at}</span>
				</div>
			</div> : name === "produits" ? <div className='d-flex' >
				{/* ====== Produits */}
				<div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Nom:</p>
						<p className='fs-5 fw-bold'>{datas?.nom}</p>
					</div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Categorie:{' '}</p> 	<span className='fs-5 fw-bold'> {donnees?.nom}</span>
					</div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Description:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.description}</span>
					</div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Fournisseur:{' '}</p> 	<span className='fs-5 fw-bold'> {fornisseur?.nom}</span>
					</div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Quantité:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.quantite}</span>
					</div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Prix:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.prix}</span>
					</div>
					<div className='d-flex'>
						<p className='fs-5 fw-bold text-secondary'>Date d'Ajout:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.updated_at}</span>
					</div>
				</div>
				<div>
					<img src={`http://127.0.0.1:8000/produit/${datas?.nameImage}`} width={200} className='img-fluid' alt="" />
				</div>
			</div> : (<>
				{/* ========== Fournisseur et Client  */}
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Nom:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.nom}</span>
				</div>
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Email:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.email}</span>
				</div>
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Téléphone:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.telephone}</span>
				</div>
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Adresse:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.adresse}</span>
				</div>
				<div className='d-flex'>
					<p className='fs-5 fw-bold text-secondary'>Date d'Ajout:{' '}</p> 	<span className='fs-5 fw-bold'> {datas?.updated_at}</span>
				</div>
			</>)}

		</>
	)
}
