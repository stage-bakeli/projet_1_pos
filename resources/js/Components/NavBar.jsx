import React, { useEffect, useState } from 'react'
import photo1 from '../../../public/assets/images/logo.png';
import photo3 from '../../../public/assets/images/faces/face28.png';
import { Link } from '@inertiajs/react';
import { Nav, Navbar } from 'react-bootstrap';

export default function NavBar({ user }) {

  const sidebar = document.getElementsByClassName("side-bar");
  const lyt = document.getElementsByClassName('layout-nav-side')
  const [show, setShow] = useState(false);

  const [activeLink, setActiveLink] = useState('home');
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    const onScroll = () => {
      if (window.scrollY > 50) {
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    }

    window.addEventListener("scroll", onScroll);

    return () => window.removeEventListener("scroll", onScroll)
  }, [])


  const handleClose = () => {
    if (show === false) {
      setShow(true);
    }
    else { setShow(false); }
  }

  useEffect(() => {
    if (show === true) {
      sidebar[0]?.classList?.add("afficher");
      lyt[0]?.classList?.remove("bien22");
    }
    else {
      sidebar[0].classList.remove("afficher");
      lyt[0].classList.add("bien22");
    }
  })



  const [data, setData] = useState(user);

  useEffect(() => {
    setData(user);
  }, []);
  return (
  
    <Navbar variant="dark" sticky="top" expand="lg" className=" d-flex justify-content-between "  id='navbar'>
      <div className=" d-flex gap-3 m-0 p-0">
        <div className="text-center  img-navbar px-3 m-0">
          <a className="" href={'http:127.0.0.1:8000/dashboard'}><img src={photo1} alt="logo" className='imf-fluid'/></a>
        </div>
        <button className="  ps-3 fs-1 py-0" onClick={handleClose} type="button" data-toggle="minimize">
          <span className="mdi mdi-menu"></span>
        </button>
        <div className="search-field d-none d-xl-block">
          <form className="d-flex align-items-center h-100" action="#">
            <div className="input-group">
              <div className="input-group-prepend bg-transparent">
                <i className="input-group-text border-0 mdi mdi-magnify"></i>
              </div>
              <input type="text" className="form-control bg-transparent border-0" placeholder="Search products" />
            </div>
          </form>
        </div>
      </div>
      <div className="d-flex gap-3 pe-3">   
        {/* <button className=" d-lg-none fs-1 " onClick={handleClose} type="button" data-toggle="offcanvas">
          <span className="mdi mdi-menu"></span>
        </button> */}
        <ul className="navbar-nav navbar-nav-right">
          <li className="nav-item nav-profile dropdown">
            <a className="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <div className="nav-profile-img">
                <img src={photo3} alt="image" />
              </div>
              <div className="nav-profile-text">
                <p className="mb-1 text-black fw-bold">{data?.prenom}</p>
              </div>
            </a>
            <div className="dropdown-menu navbar-dropdown dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="profileDropdown" data-x-placement="bottom-end">
              <div className="p-3 text-center bg-primary">
                <img className="img-avatar img-avatar48 img-avatar-thumb" src={photo3} alt="" />
              </div>
              <div className="p-2">
                <h5 className="dropdown-header text-uppercase pl-2 text-dark">User Options</h5>

                <a className="dropdown-item py-1 d-flex align-items-center justify-content-between" href={route('profile.edit')}>
                  <span>Profile</span>
                  <span className="p-0">
                    <i className="mdi mdi-account-outline ml-1"></i>
                  </span>
                </a>

                <div role="separator" className="dropdown-divider"></div>

                <Link className="dropdown-item py-1 d-flex align-items-center justify-content-between" href={route('logout')} method="POST">
                  <span>Log Out</span>
                  <i className="mdi mdi-logout ml-1"></i>
                </Link>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </Navbar>
  )
}
