import { InertiaLink } from '@inertiajs/inertia-react'
import React from 'react'
import photo1 from '../../../public/assets/images/logo.png';
import { SiHomeassistantcommunitystore } from 'react-icons/si'
import { TbCategory } from 'react-icons/tb'
import { AiOutlineOrderedList } from 'react-icons/ai'
import { MdLocalGroceryStore } from 'react-icons/md'
import { SiEventstore } from 'react-icons/si'
import { BsFillPersonLinesFill } from 'react-icons/bs'
// import photo2 from '../../../public/assets/images/faces/face28.png';

export default function SideBar() {
	const name = ["categories", "produits", "fornisseurs", "clients","commande"]



	return (

		<nav className="sidebar  side-bar" id="sidebar">
			<div className="d-flex  justify-content-center ">
				<a className="navbar-brand brand-logo" href={'http:127.0.0.1:8000/dashboard'}><img src={photo1} alt="logo" className='img-sidebar'/></a>
			</div>
			<ul className="nav">
				<li className="nav-item ps-3">
					<a className="m-0 nav-link d-flex d-md-none" href={'http://127.0.0.1:8000/dashboard'}>
						<span className="fs-1"><SiHomeassistantcommunitystore /></span>
					</a>
					<a className="nav-link d-none d-md-flex" href={'http://127.0.0.1:8000/dashboard'}>
						<span className="icon-bg"><i className="mdi mdi-cube menu-icon"></i></span>
						<span className="menu-title">Dashboard</span>
					</a>
				</li>
				<li className="nav-item ps-3">
					<InertiaLink className="nav-link d-flex d-md-none" href={`/donnees/${name[0]}`}>
						<span className="fs-1"><TbCategory /></span>
					</InertiaLink>
					<a className="nav-link d-none d-md-flex" href={`/donnees/${name[0]}`}>
						<span className="icon-bg"><i className="mdi mdi-crosshairs-gps menu-icon"></i></span>
						<span className="menu-title">Categories</span>
					</a>
				</li>
				<li className="nav-item ps-3">
					<InertiaLink className="nav-link d-flex d-md-none" href={`/donnees/${name[3]}`}>
						<span className="fs-1"><BsFillPersonLinesFill /></span>
					</InertiaLink>
					<a className="nav-link d-none d-md-flex" href={`/donnees/${name[3]}`} >
						<span className="icon-bg"><i className="mdi mdi-contacts menu-icon"></i></span>
						<span className="menu-title">Clients</span>
					</a>
				</li>
				<li className="nav-item ps-3">
					<InertiaLink className="nav-link d-flex d-md-none" href={`/donnees/${name[2]}`}>
						<span className="fs-1">  <SiEventstore /></span>
					</InertiaLink>
					<a className="nav-link d-none d-md-flex" href={`/donnees/${name[2]}`}>
						<span className="icon-bg"><i className="mdi mdi-format-list-bulleted menu-icon"></i></span>
						<span className="menu-title">Fornisseurs</span>
					</a>
				</li>
				<li className="nav-item ps-3">
					<InertiaLink className="nav-link d-flex d-md-none" href={`/donnees/${name[1]}`}>
						<span className="fs-1"><MdLocalGroceryStore /></span>
					</InertiaLink>
					<a className="nav-link d-none d-md-flex" href={`/donnees/${name[1]}`}>
						<span className="icon-bg"><i className="mdi mdi-table-large menu-icon"></i></span>
						<span className="menu-title ">Produits</span>
					</a>
				</li>
				<li className="nav-item ps-3">
					<InertiaLink className="nav-link d-flex d-md-none" href={`/donnees/${name[4]}`}>
						<span className="fs-1"><AiOutlineOrderedList /></span>
					</InertiaLink>
					<a className="nav-link d-none d-md-flex" href={`/donnees/${name[4]}`}>
						<span className="icon-bg"><i className="mdi mdi-menu menu-icon"></i></span>
						<span className="menu-title ">Commandes</span>
					</a>
				</li>
			</ul>
		</nav>


		// <div id="" className=" container-fluid side-bar">
		// 	<div className="col-auto side-menu">
		// 		<ul className="">

		// 			<li className="p-0 side-link  ">
		// 				<a className="m-0 nav-link d-flex" href={'http://127.0.0.1:8000/dashboard'}>
		// 					<span className="icon-bg"><SiHomeassistantcommunitystore /></span>
		// 					<span className="menu-title hideTitle">Dashboard</span>
		// 				</a>
		// 			</li>
		// 			<li className=" side-link ">
		// 				<InertiaLink className="nav-link d-flex" href={`/donnees/${name[0]}`}>
		// 					<span className="icon-bg"><TbCategory /></span>
		// 					<span className="menu-title hideTitle">Categories</span>
		// 				</InertiaLink>
		// 			</li>
		// 			<li className=" side-link  ">
		// 				<InertiaLink className="nav-link d-flex" href={`/donnees/${name[1]}`}>
		// 					<span className="icon-bg"><MdLocalGroceryStore /></span>
		// 					<span className="menu-title hideTitle">Produits</span>
		// 				</InertiaLink>
		// 			</li>
		// 			<li className=" side-link  ">
		// 				<InertiaLink className="nav-link d-flex" href={`/donnees/${name[2]}`}>
		// 					<span className="icon-bg">  <SiEventstore /></span>
		// 					<span className="menu-title hideTitle">Fornisseurs</span>
		// 				</InertiaLink>
		// 			</li>
		// 			<li className=" side-link  ">
		// 				<InertiaLink className="nav-link d-flex" href={`/donnees/${name[3]}`}>
		// 					<span className="icon-bg"><BsFillPersonLinesFill /></span>
		// 					<span className="menu-title hideTitle">Clients</span>
		// 				</InertiaLink>
		// 			</li>
		// 		</ul>
		// 	</div>
		// </div>
	)
}

