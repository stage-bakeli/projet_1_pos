import React, { useEffect, useState } from 'react'
import getLocStorage from '../ExportComponent';
import fetchData from '../fecthData';
import { useForm } from 'react-hook-form';
import Swal from 'sweetalert2';
import axios from 'axios';

export default function Commande({ setDataPanier, dataPanier, name, my_data_list, user_id }) {

	const [dataClient, setDataClient] = useState()

	const arr = [];
	for (const key in my_data_list) {
		if (Object.hasOwnProperty.call(my_data_list, key)) {
			arr.push({
				id: my_data_list[key].data.id,
				nom: my_data_list[key].data.nom,
				image: my_data_list[key].data.image,
				prix: my_data_list[key].data.prix,
				categorie: my_data_list[key].data.categorie,
				occurrences: my_data_list[key].occurrences
			});
		}
	}

	console.log(dataClient);
	
	const annuleCommand = () => {
		localStorage.removeItem("produits")

		Swal.fire({
			text: "Commende annuler!",
			timer: 1000,
			timerProgressBar: true,
			showConfirmButton: false,
		})
		getLocStorage(setDataPanier);

	}

	const prixTotal = dataPanier?.reduce((total, produit) => {
		return total + produit?.prix;
	}, 0);


	useEffect(() => {
		fetchData(setDataClient, name);
	}, [])



	const { register, handleSubmit, watch } = useForm();

	const onSubmit = (data) => {

		const { id_client, payer } = data;
		const donnees = {
			user_id: user_id,
			id_client: id_client,
			my_liste: arr,
			payer: payer,
			total: prixTotal,
			nombreArticle: dataPanier?.length
		}

		console.log(donnees)

		axios.post(`http://127.0.0.1:8000/api/commande`, donnees).then(({ data }) => {
			console.log(data)
			Swal.fire({
				icon: "success",
				text: data.message
			})
			localStorage.removeItem("produits");
			getLocStorage(setDataPanier);
		}).catch(({ response }) => {
			if (response?.status === 422) {
				console.log(response.data.errors)
			} else {
				Swal.fire({
					text: response.data.message,
					icon: "error"
				})
			}
		})
	};


	return (
		<form onSubmit={handleSubmit(onSubmit)}>

			<ul className='list-group d-flex flex-column gap-1'>
				<li className="list-group-item d-flex justify-content-between align-items-start">
					<div className="ms-2 me-auto">
						<div className="fw-bold">Articles ajouter</div>
					</div>
					<span className="badge bg-primary rounded-pill">{dataPanier?.length}</span>
				</li>
				<li className="list-group-item d-flex justify-content-between align-items-start">
					<div className="ms-2 me-auto">
						<div className="fw-bold">Prix total</div>
					</div>
					<span className="badge bg-primary rounded-pill">{prixTotal} fcaf</span>
				</li>
				<li className="list-group-item d-flex justify-content-between align-items-start">
					<select  {...register('id_client')}>
						<option value="">Selection un client</option>
						{dataClient?.map((item, index) => {
							return <option value={item.id} key={index}>{item.nom}</option>
						})}
					</select>
					<div class="form-switch d-flex">
						<label class="fw-bold" for="flexSwitchCheckDefault">Payer</label>
						<input {...register('payer')} class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" />
					</div>

				</li>
				<li className="list-group-item d-flex justify-content-between align-items-start">
					<button className="btn btn-md btn-danger" onClick={() => annuleCommand()}>Annuler</button>


					<button type="submit" className='btn bg-success btn-success'>Valider</button>
				</li>
			</ul>
		</form>
	)
}
