import "./Cards.css";
import React, { useEffect, useState } from "react";
import photo3 from '../../../../public/assets/images/faces/face28.png';
import axios from "axios";
import { MdAddShoppingCart } from "react-icons/md";
import { Col, Container, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import getLocStorage from "../ExportComponent";


function Cards({setData}) {

  
  useEffect(() => {
    fetchProducts()
  }, [])
  
  const fetchProducts = async () => {
    await axios.get(`http://localhost:8000/api/produits`).then(({ data }) => {
      setProducts(data)
    })
  }
  
  const [products, setProducts] = useState([])

  const addlocalStrage = (data) => {
    const storedProduits = localStorage.getItem("produits");
    const produits = JSON.parse(storedProduits) || [];
    produits.push({
      id: data.id,
      nom: data.nom,
      image: data.images,
      prix: data.prix,
      categorie: data.id_category,
    });
    localStorage.setItem("produits", JSON.stringify(produits));
    Swal.fire({
      icon: "success",
      text: "Article ajouté au panier!",
      timer: 2000,
      timerProgressBar: true,
      showConfirmButton: false,
    });
    getLocStorage(setData);
  };


  


  return (
    <Container fluid className="py-4">
      <Row className="p-0 gap-2  mx-auto">
        {products.map((item, index) => {
          return <Col md={3} key={index} className="p-0 cardProduit">
            <div className="proj-imgbx">
              <img src={`produit/${item?.images}`} className="img-fluid" alt="Mes cards de produits" />
              <div className="proj-txtx ">
                <span>{item?.description}</span>
                <div className="d-flex justify-content-center pt-3 align-items-center">
                  <button className="btn btn-md d-flex" onClick={()=>addlocalStrage(item)}><MdAddShoppingCart /> <span> Ajouter </span>   </button>
                </div>
              </div>
            </div>
            <div className="py-2 ">
              <h4 className="text-center fw-bold text-uppercase">{item?.nom}</h4>
              <div className="d-flex justify-content-between px-2">
                <p className="fw-semibold">Prix :</p> <strong>{item.prix} fcfa</strong>
              </div>
            </div>
          </Col>
        })}
      </Row>
    </Container>
  );
};

export default Cards;
