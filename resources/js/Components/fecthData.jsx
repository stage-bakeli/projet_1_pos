export default async function fetchData(setProducts, name ) {
	try {
	  const response = await axios.get(`http://localhost:8000/api/${name}`);
	  setProducts(response.data);
	} catch (error) {
	  console.error(error);
	}
  }
  