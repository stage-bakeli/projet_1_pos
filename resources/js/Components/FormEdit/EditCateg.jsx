import { useForm } from '@inertiajs/inertia-react';
import React, { useEffect, useState } from 'react';
import InputLabel from '../InputLabel';
import TextInput from '../TextInput';
import PrimaryButton from '../PrimaryButton';
import Swal from 'sweetalert2';

export default function EditCateg({ name, id }) {

  const [isValid, setIsValid] = useState('');
  const [descError, setDescError] = useState('');
  const [nomError, setNomError] = useState('');
  const [donnees, setDonnees] = useState(null);

  const fetchProducts = async () => {
    await axios.get(`http://localhost:8000/api/${name}/${id}`).then(({ data }) => {
      setDonnees(data.data);
    })
  }

  useEffect(() => {
    fetchProducts()
  }, [])



  const { data, setData, errors, processing, setDefaultData, register } = useForm({
    nom: '',
    description: '',
  });

  useEffect(() => {
    if (donnees) {
      setData({
        nom: donnees.nom,
        description: donnees.description,
      });
    }
  }, [donnees, setDefaultData]);



  useEffect(() => {
    const regexNom = /^.{5,30}$/;
    const isNomValid = regexNom.test(data.nom);
    setNomError(isNomValid ? '' : 'Le nom doit avoir entre 5 et 30 caractères');

    const regexDesc = /^.{10,500}$/;
    const isDescValid = regexDesc.test(data.description);
    setDescError(isDescValid ? '' : 'Faites une description de la catégorie d\' au moin 10 caratères');

    setIsValid(isNomValid && isDescValid);
  }, [data]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (isValid === true) {
      const url = `http://localhost:8000/api/${name}/${id}`;
      await axios.put(url, data).then(({ data }) => {
        Swal.fire({
          icon: "success",
          text: data.message
        })
        window.location.replace(`http://localhost:8000/donnees/${name}`)
        reset();
      }).catch(({ response }) => {
        if (response?.status === 422) {
          setValidationError(response.data.errors)
        } else {
          Swal.fire({
            text: response.data.message,
            icon: "error"
          })
        }
      })
    }

  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <InputLabel htmlFor='nom' value='Nom' />
        <TextInput
          id="nom"
          name="nom"
          value={data?.nom}
          className="mt-1 block w-full"
          autoComplete="nom"
          isFocused={true}
          onChange={(e) => setData('nom', e.target.value)}
          ref={register}
        />
        {nomError && <p className='text-danger'>{nomError}</p>}
      </div>
      <div>
        <InputLabel htmlFor='description' value='Description' />
        <textarea
          id="description"
          name="description"
          value={data?.description}
          className="mt-1 block w-full"
          autoComplete="description"
          onChange={(e) => setData('description', e.target.value)}
          ref={register}
        />
        {descError && <p className='text-danger'>{descError}</p>}
      </div>
      <div className='sm:flex justify-center py-2.5'>
        <PrimaryButton className="ml-4 py-2.5 text-uppercase" disabled={processing}>
          Mis à jour {name}
        </PrimaryButton>
      </div>
    </form>
  );
}
