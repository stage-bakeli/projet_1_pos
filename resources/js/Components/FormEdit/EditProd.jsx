import React, { useEffect, useState } from 'react'
import InputLabel from '../InputLabel'
import TextInput from '../TextInput'
import InputError from '../InputError'
import PrimaryButton from '../PrimaryButton'
import { useForm } from '@inertiajs/react'
import Swal from 'sweetalert2'
import axios from 'axios'

export default function EditProd({ name, id }) {
	const [donneesFour, setDonneesFour] = useState();
	const [donnees, setDonnees] = useState()
	const [donneesPrd, setDonneesPrd] = useState()
	// state regex
	const [isValid, setIsValid] = useState('');
	const [catgError, setCatgError] = useState('');
	const [nomError, setNomError] = useState('');
	const [fourError, setFourError] = useState('');
	const [quantiteError, setQuantiteError] = useState('');
	const [descError, setDescError] = useState('');
	const [prixError, setPrixError] = useState('');


	const fetchProducts = async () => {
		await axios.get(`http://localhost:8000/api/categories`).then(({ data }) => {
			setDonnees(data)
		})
		await axios.get(`http://localhost:8000/api/fornisseurs`).then(({ data }) => {
			setDonneesFour(data)
		})
		await axios.get(`http://localhost:8000/api/${name}/${id}`).then(({ data }) => {
			setDonneesPrd(data.data);
		})
	}
	useEffect(() => {
		fetchProducts()
	}, [])



	const url = `http://localhost:8000/api/produits/${id}`;

	const { data, setData, errors, processing, setDefaultData } = useForm({
		id_category: '', nom: '', id_fournisseur: '', quantite: '', description: '', prix: '', image: null
	});

	useEffect(() => {
		if (donneesPrd) {
			setData({
				id_category: donneesPrd.id_category,
				nom: donneesPrd.nom,
				id_fournisseur: donneesPrd.id_fournisseur,
				quantite: donneesPrd.quantite,
				description: donneesPrd.description,
				prix: donneesPrd.prix,
				image: donneesPrd.image,
			});
		}
	}, [donneesPrd, setDefaultData]);

	useEffect(() => {
		const regexCat = /^\d+$/;
		const isCatValid = regexCat.test(data.id_category);
		setCatgError(isCatValid ? '' : 'Ce champs est obligatoire');

		const regexNom = /^.{5,30}$/;
		const isNomValid = regexNom.test(data.nom);
		setNomError(isNomValid ? '' : 'Le nom doit avoir entre 5 et 30 caractères');

		const regexFour = /^\d+$/;
		const isFourValid = regexFour.test(data.id_fournisseur);
		setFourError(isFourValid ? '' : 'Ce champs est obligatoire');

		const regexQuantite = /^\d+$/;
		const isQuantiteValid = regexQuantite.test(data.quantite);
		setQuantiteError(isQuantiteValid ? '' : 'Ce champs est obligatoire');

		const regexDesc = /^.{10,500}$/;
		const isDescValid = regexDesc.test(data.description);
		setDescError(isDescValid ? '' : 'Faites une description du produits d\' au moin 10 caratères');

		const regexPrix = /^\d+$/;
		const isPrixValid = regexPrix.test(data.prix);
		setPrixError(isPrixValid ? '' : 'Ce champs est obligatoire');



		setIsValid(isNomValid && isDescValid && isCatValid && isPrixValid && isQuantiteValid && isFourValid);
	}, [data]);

	const submit = async (e) => {
		e.preventDefault();

		if (isValid === true) {
			const formData = new FormData();
			formData.append('id_category', data?.id_category);
			formData.append('nom', data?.nom);
			formData.append('id_fournisseur', data?.id_fournisseur);
			formData.append('quantite', data?.quantite);
			formData.append('description', data?.description);
			formData.append('prix', data?.prix);
			formData.append('image', data?.image);

			try {
				const response = await axios.put(url, formData);
				console.log(response.data);
				Swal.fire({
					icon: "success",
					text: response.data.message
				});
				//   window.location.replace(`http://127.0.0.1:8000/donnees/${name}`);
				//   reset();
			} catch (error) {
				if (error.response?.status === 422) {
					console.log(error.response.data.errors);
				} else {
					Swal.fire({
						text: error.response?.data?.message || "Une erreur s'est produite",
						icon: "error"
					});
				}
			}
		}
	}





	return (



		<form onSubmit={submit} >

			<div>
				<InputLabel htmlFor='id_category' value='Categories' />
				<select value={data?.id_category} onChange={(e) => setData('id_category', e.target.value)} required>
					<option>choisissez une categorie</option>
					{donnees?.map((item, index) => {
						return <option id="id_category" key={index}
							name="id_category" value={item?.id}
						>{item?.nom}</option>
					})}

				</select>
				{catgError && <p className='text-danger'>{catgError}</p>}
			</div>
			<div>
				<InputLabel htmlFor='nom' value='Nom' />
				<TextInput
					id="nom"
					name="nom"
					value={data?.nom}
					className="mt-1 block w-full"
					autoComplete="nom"
					isFocused={true}
					onChange={(e) => setData('nom', e.target.value)}
					required
				/>

				{nomError && <p className='text-danger'>{nomError}</p>}
			</div>
			<div>
				<InputLabel htmlFor='id_fournisseur' value='Fournisseur' />
				<select value={data?.id} onChange={(e) => setData('id_fournisseur', e.target.value)} required>
					<option>choisissez un fournisseur</option>
					{donneesFour?.map((item, index) => {
						return <option id="id_fournisseur" key={index}
							name="id_fournisseur" value={item?.id}
						>{item?.nom}</option>
					})}
				</select>
				{fourError && <p className='text-danger'>{fourError}</p>}
			</div>

			<div>
				<InputLabel htmlFor='quantite' value='Quantite' />
				<TextInput
					id="quantite"
					name="quantite"
					value={data.quantite}
					className="mt-1 block w-full"
					autoComplete="quantite"
					isFocused={true}
					onChange={(e) => setData('quantite', e.target.value)}
					required
				/>

				{quantiteError && <p className='text-danger'>{quantiteError}</p>}
			</div>
			<div>
				<InputLabel htmlFor='description' value='Description' />
				<textarea
					id="description"
					name="description"
					value={data?.description}
					className="mt-1 block w-full"
					autoComplete="description"
					onChange={(e) => setData('description', e.target.value)}
					required
				/>
				{descError && <p className='text-danger'>{descError}</p>}
			</div>
			<div>
				<InputLabel htmlFor='prix' value='Prix' />
				<TextInput
					id="prix"
					name="prix"
					value={data.prix}
					className="mt-1 block w-full"
					autoComplete="prix"
					isFocused={true}
					onChange={(e) => setData('prix', e.target.value)}
					required
				/>
				{prixError && <p className='text-danger'>{prixError}</p>}
			</div>
			<div>
				<InputLabel htmlFor='image' value='Image' />
				<input
					type="file"
					className="w-full px-4 py-2"
					label="image"
					name="image"
					onChange={(e) =>
						setData("image", e.target.files[0])
					}
				
				/>

				<InputError message={errors.image} className="mt-2 text-danger" />
			</div>
			<div className='sm:flex justify-center py-2.5'>
				<PrimaryButton className="ml-4 py-2.5 text-uppercase" disabled={processing}>
					Ajout {name}
				</PrimaryButton>
			</div>
		</form>
	)
}