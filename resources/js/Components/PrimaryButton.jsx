export default function PrimaryButton({ className = '', disabled, children, ...props }) {
    return (
        <button
            {...props}
            className='btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn'
            disabled={disabled}
        >
            {children}
        </button>
    );
}
