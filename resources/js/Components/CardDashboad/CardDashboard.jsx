import React from 'react'

export default function CardDashboard(titre, icon, nombre, couleur) {
	return (

		<div className="cards-admin py-2" >
			<div className='card' style={{ borderLeft: `5px solid ${couleur}`, objectFit: "cover", color: `${couleur}` }}>
				<div className='cardbody'>
					<div className="d-flex justify-content-between">
						<h5 className="text-uppercase">{titre}</h5>
						{icon}
					</div>
					<div>
						<span className="h1" style={{ color: '#18113c' }}>{nombre}</span>
					</div>
				</div>
			</div>
		</div>
	)
}
