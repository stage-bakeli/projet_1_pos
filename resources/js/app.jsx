import './bootstrap';
import '../css/app.css';
import ReactDOM from 'react-dom';

import { createRoot } from 'react-dom/client';
import { createInertiaApp } from '@inertiajs/react';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import Create from './Pages/Donnees/Create';
import { BrowserRouter as Router } from 'react-router-dom';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.jsx`, import.meta.glob('./Pages/**/*.jsx')),
    setup({ el, App, props }) {
        const root = createRoot(el);

        root.render(<Router><App {...props} /></Router> );
    },
    progress: {
        color: '#4B5563',
    },
});

// ReactDOM.render(<Create />, document.getElementById('my-react-app'));