import React, { useState, useEffect } from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import axios from 'axios';
import { TbCategory } from 'react-icons/tb';
import { MdLocalGroceryStore } from 'react-icons/md';
import { TiShoppingCart, TiCancel } from 'react-icons/ti';
import { FcApproval } from 'react-icons/fc';
import { SiEventstore } from 'react-icons/si';
import { BsFillPersonLinesFill } from 'react-icons/bs';
import Cards from '@/Components/Cards/Cards';
import ListePanier from '@/Components/Listes/ListePanier';
import Swal from 'sweetalert2';
import getLocStorage from '@/Components/ExportComponent';
import Commande from '@/Components/Commande/Commande';

export default function Dashboard({ auth }) {

  const [dataCetg, setDataCetg] = useState([]);
  const [dataProd, setDataProd] = useState([]);
  const [dataFour, setDataFour] = useState([]);
  const [dataCli, setDataCli] = useState([]);
  const [dataPanier, setDataPanier] = useState([]);

  const getData = async () => {
    try {
      const { data: categories } = await axios.get('http://localhost:8000/api/categories');
      setDataCetg(categories);

      const { data: produits } = await axios.get('http://localhost:8000/api/produits');
      setDataProd(produits);

      const { data: fournisseurs } = await axios.get('http://localhost:8000/api/fornisseurs');
      setDataFour(fournisseurs);

      const { data: clients } = await axios.get('http://localhost:8000/api/clients');
      setDataCli(clients);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const cardData = [
    { titre: 'Catégories', icon: <TbCategory />, nombre: dataCetg ? dataCetg.length : "", couleur: '#18113c' },
    { titre: 'Produits', icon: <MdLocalGroceryStore />, nombre: dataProd ? dataProd.length : "", couleur: '#ff0404' },
    { titre: 'Fournisseurs', icon: <SiEventstore />, nombre: dataFour ? dataFour.length : "", couleur: '#0877bd' },
    { titre: 'Clients', icon: <BsFillPersonLinesFill />, nombre: dataCli ? dataCli.length : "", couleur: '#7b0ac2f2' },
  ]




  useEffect(() => {
    getLocStorage(setDataPanier);
  }, []);








  const uniqueProduits = dataPanier.reduce((acc, produit) => {
    if (acc[produit.id]) {
      acc[produit.id].occurrences += 1;
    } else {
      acc[produit.id] =
      {
        data: produit,
        occurrences: 1,
      }
        ;
    }
    return acc;
  }, {});


  return (
    <AuthenticatedLayout
      user={auth?.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>
      }
    >
      <Head title="Dashboard" />
      <div className='container-fluid'>
        <div className=" m-0 px-2 ">
          <div className="row py-1 ">
            {cardData.map((item, index) => {
              return <div className="cards-admin py-2 col-md-3" key={index}>
                <div className='card cardDash p-2' style={{
                  borderRight: `1px solid ${item.couleur}`, borderTop: `1px solid ${item.couleur}`,
                  borderBottom: `1px solid ${item.couleur}`,
                  borderLeft: `8px solid ${item.couleur}`, color: `${item.couleur}`
                }}>
                  <div className='cardbody'>
                    <div className="d-flex justify-content-between">
                      <h5 className="text-uppercase">{item.titre}</h5>
                      {item.icon}
                    </div>
                    <div>
                      <span className="h1" style={{ color: '#18113c' }}>{item.nombre}</span>
                    </div>
                  </div>
                </div>
              </div>
            })}
          </div>
          <h2 className="text-center fs-2">Faites vos ventes</h2>
        </div>
        <div className='d-flex'>
          <Cards setData={setDataPanier} />
          <div className="sticky-top panier" >
            <div className="d-flex justify-content-between">
              <h3 className='fw-bold fs-3'>Votre panier</h3>
              <div className='icon-panier '>
                <TiShoppingCart className='fs-1 text-primary' />
                <div>
                  <span className='nbrPanier badge bg-danger fw-bolde rounded-pill '>{dataPanier?.length}</span>
                </div>
              </div>
            </div>
            <hr />
            <ListePanier data={uniqueProduits} setData={setDataPanier} />
            <hr />
            <div className='py-2'>
              <Commande my_data_list={uniqueProduits} setDataPanier={setDataPanier} dataPanier={dataPanier} name='clients' user_id={auth.user.id}/>
            </div>
          </div>
        </div>

      </div>

    </AuthenticatedLayout>
  );
}
