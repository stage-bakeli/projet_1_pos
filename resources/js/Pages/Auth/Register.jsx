import { useEffect } from 'react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';

export default function Register() {
    const { data, setData, post, processing, errors, reset } = useForm({
        name: '',
        prenom: '',
        addresse: '',
        email: '',
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('register'));
    };

    return (
        <GuestLayout>
            <Head title="Register" />

            <form onSubmit={submit}>
                <div>
                    <InputLabel htmlFor="name" value="Nom" />

                    <TextInput
                        id="name"
                        name="name"
                        value={data.name}
                        className="mt-1 block w-full"
                        autoComplete="name"
                        isFocused={true}
                        onChange={(e) => setData('name', e.target.value)}
                    />

                    <InputError message={errors.name} className="mt-2 text-danger" />
                </div>

                <div>
                    <InputLabel htmlFor="prenom" value="prenom" />

                    <TextInput
                        id="prenom"
                        name="prenom"
                        value={data.prenom}
                        className="mt-1 block w-full"
                        autoComplete="prenom"
                        isFocused={true}
                        onChange={(e) => setData('prenom', e.target.value)}
                    />

                    <InputError message={errors.prenom} className="mt-2 text-danger" />
                </div>

                <div>
                    <InputLabel htmlFor="addresse" value="addresse" />

                    <TextInput
                        id="addresse"
                        name="addresse"
                        value={data.addresse}
                        className="mt-1 block w-full"
                        autoComplete="addresse"
                        isFocused={true}
                        onChange={(e) => setData('addresse', e.target.value)}
                    />

                    <InputError message={errors.addresse} className="mt-2 text-danger" />
                </div>

                <div className="mt-4">
                    <InputLabel htmlFor="email" value="Email" />

                    <TextInput
                        id="email"
                        type="email"
                        name="email"
                        value={data.email}
                        className="mt-1 block w-full"
                        autoComplete="username"
                        onChange={(e) => setData('email', e.target.value)}
                    />

                    <InputError message={errors.email} className="mt-2 text-danger" />
                </div>

                <div className="mt-4">
                    <InputLabel htmlFor="password" value="Mot de passe" />

                    <TextInput
                        id="password"
                        type="password"
                        name="password"
                        value={data.password}
                        className="mt-1 block w-full"
                        autoComplete="new-password"
                        onChange={(e) => setData('password', e.target.value)}
                    />

                    <InputError message={errors.password} className="mt-2 text-danger" />
                </div>

                <div className="mt-4">
                    <InputLabel htmlFor="password_confirmation" value="Cnfirmé le mot de passe" />

                    <TextInput
                        id="password_confirmation"
                        type="password"
                        name="password_confirmation"
                        value={data.password_confirmation}
                        className="mt-1 block w-full"
                        autoComplete="new-password"
                        onChange={(e) => setData('password_confirmation', e.target.value)}
                    />

                    <InputError message={errors.password_confirmation} className="mt-2 text-danger" />
                </div>

                <div className="flex justify-end mt-4">
                    <a
                        href="/" className="ml-4 text-primary"
                    >
                        Vous existez deja?
                    </a>

                </div>
                <div>
                    <PrimaryButton disabled={processing} className='fw-bold'>
                        S'incrire
                    </PrimaryButton>
                </div>
            </form>
        </GuestLayout>
    );
}
