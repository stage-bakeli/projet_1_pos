import React from 'react'
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head,  usePage } from '@inertiajs/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import ShowCateg from '@/Components/ShowDonne/ShowCateg';
import ShowCommande from '@/Components/ShowDonne/ShowCommande';

export default function Show({ auth }) {

	const { name } = usePage().props;
	const { id } = usePage().props;
	const [donnees, setDonnees] = useState()
	const [dataCateg, setDataCateg] = useState()
	const [dataFour, setDataFour] = useState()

	
	const fetchProducts = async () => {
		await axios.get(`http://localhost:8000/api/${name}/${id}`).then(({ data }) => {
			setDonnees(data.data);
			fetchCategorie(data.data.id_category)
			fetchFournisseur(data.data.id_fournisseur)
		})
	}

	const fetchCategorie = async (cat_id) => {
		await axios.get(`http://localhost:8000/api/categories/${cat_id}`).then(({ data }) => {
			setDataCateg(data.data)
		})
	}

	const fetchFournisseur = async (four_id) => {
		await axios.get(`http://localhost:8000/api/fornisseurs/${four_id}`).then(({ data }) => {
			setDataFour(data.data)
		})
	}

	
	useEffect(() => {
		fetchProducts()
	}, [])

	return (
		<AuthenticatedLayout
			user={auth?.user}
			header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>
			}
		>
			<Head title="Données" />
			{name==='commande'?<ShowCommande id={id} name={name}  />:
			<div className="py-12">
				<div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
					<div className="bg-white overflow-hidden shadow-sm sm:rounded-lg flex justify-between">
						<div className='p-6 text-dark'>
							<h1 className='fs-2 text-uppercase' >{name}</h1>
							
							<ShowCateg datas={donnees} name={name} donnees={dataCateg} fornisseur={dataFour}/>
						</div>
					</div>
				</div>
			</div>}
		</AuthenticatedLayout>
	)
}
