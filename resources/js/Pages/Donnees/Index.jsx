import React from 'react'

import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, usePage } from '@inertiajs/react';
import { InertiaLink } from '@inertiajs/inertia-react';
import ListesCateg from '@/Components/Listes/ListesCateg';
import ListesProd from '@/Components/Listes/ListesProd';
import ListesFrCl from '@/Components/Listes/ListesFrCl';
import ListeCommande from '@/Components/Listes/ListeCommande';

export default function Index({ auth }) {
  const { name } = usePage().props;

  return (
    <AuthenticatedLayout
      user={auth?.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Données</h2>
      }
    >
      <Head title="Données" />
      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg flex justify-between">
            <div className="p-6 fs-3 font-serif font-bold text-gray-900">Liste des {name}</div>
            <div className='p-6'>
              {
                name !=='commande'? <button type="button" className='btn btn-md text-white bg-success' >
                <InertiaLink className="nav-link" href={`/donnees/${name}/create`}>
                  <span className="icon-bg"><i className="mdi mdi-cube menu-icon"></i></span>
                  <span className="menu-title">Ajout un(e) {name}</span>
                </InertiaLink>
              </button> : null
              }
              


              <div className="modal fade " id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog bg-white">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h1 className="modal-title text-dark fw-bold fs-5" id="exampleModalLabel">Ajout Produit</h1>
                    </div>
                    <div className="modal-body text-dark">

                    </div>
                    <div className="modal-footer">
                      <button type="button" className="btn fs-6 bg-danger text-white" data-bs-dismiss="modal">Close</button>
                      <button type="button" className="btn fs-6 bg-success text-white">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {name==="categories"?<ListesCateg  name={name} />: name==='produits'?<ListesProd  name={name} />:name==='commande'?<ListeCommande name={name} />:<ListesFrCl name={name}/>}
        </div>
      </div>
    </AuthenticatedLayout>
  )
}
