import React, { useState } from 'react'

import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout'
import InputLabel from '@/Components/InputLabel'
import TextInput from '@/Components/TextInput'
import { Head, useForm, usePage } from '@inertiajs/react'
import InputError from '@/Components/InputError'
import PrimaryButton from '@/Components/PrimaryButton'
import FormCateg from '@/Components/Formulaire/FormCateg'
import FormProd from '@/Components/Formulaire/FormProd'

export default function Create({ auth }) {
	const { name } = usePage().props;


	return (
		<div>
			<AuthenticatedLayout
				user={auth.user}
				header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Ajouter un produit</h2>
				}
			>
				<Head title="Create" />

				<div className="p-2">
					<div className="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
						<div className="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
							<div className="p-6 fs-2 text-center fw-bold text-gray-900">Ajout {name}</div>
							{name === 'produits' ? <FormProd name={name} user_id={auth.user.id}/> : <FormCateg name={name} user_id={auth.user.id}/>}
						</div>

					</div>
				</div>

			</AuthenticatedLayout>
		</div>
	)
}
