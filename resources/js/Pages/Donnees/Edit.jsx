import React from 'react'
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import Listes from '@/Components/Listes/ListesFrCl';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, useForm, usePage } from '@inertiajs/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import EditCateg from '@/Components/FormEdit/EditCateg';
import EditProd from '@/Components/FormEdit/EditProd';
import EditFrCl from '@/Components/FormEdit/EditFrCl';

export default function Edit({ auth }) {

	const [datas, setDatas] = useState();

	const { name } = usePage().props;
	const { id } = usePage().props;



	const url = `http://127.0.0.1:8000/api/${name}`;

	const getData = async () => {
		await axios.get(url).then(
			(response) => {
				const data = response.data?.filter((i) => i.id == id);
				setDatas(data)
			}
		)
	}

	useEffect(() => {
		getData();
	}, []);

	return (
		<div>
			<AuthenticatedLayout
				user={auth.user}
				header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Ajouter un produit</h2>
				}
			>
				<Head title="Edit" />
				<div className="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">

					<div className="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
						<div className="p-6 text-gray-900">Mise à jour </div>
						{name==='categories'?<EditCateg datas={datas} name={name} id={id}/>:name==="produits"?<EditProd datas={datas} name={name} id={id}/>:
						<EditFrCl datas={datas} name={name} id={id}/>}			
					</div>
				</div>
			</AuthenticatedLayout>
		</div>
	)
}
