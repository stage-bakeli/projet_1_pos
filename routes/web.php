<?php

use App\Http\Controllers\DonneController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Login', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');



Route::get('/donnees/{name}', function ($name) {
    return Inertia::render('Donnees/Index', ['name' => $name]);
})->middleware(['auth', 'verified'])->name('index');

Route::get('/donnees/{name}/create', function ($name) {
    return Inertia::render('Donnees/Create', ['name' => $name]);
})->middleware(['auth', 'verified'])->name('create');

Route::get('/donnees/{name}/{id}/show', function ($name, $id) {
    return Inertia::render('Donnees/Show', ['name' => $name , 'id'=> $id]);
})->middleware(['auth', 'verified'])->name('show');

Route::get('/donnees/{name}/{id}/edit', function ($name, $id) {
    return Inertia::render('Donnees/Edit', ['name' => $name, 'id'=> $id]);
})->middleware(['auth', 'verified'])->name('edit');



// Route::middleware('auth')->group(function () {
//     Route::get('/donnees/{name}', [DonneController::class, 'index'])->name('donne');
//     Route::patch('/donnees/{name}/{id}/show', [DonneController::class, 'show'])->name('profile.update');
//     Route::delete('/donnees/{name}/{id}/edit', [DonneController::class, 'edit'])->name('profile.destroy');
// });

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
