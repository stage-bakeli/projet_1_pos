<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'my_liste',
        'id_client',
        'payer',
        'total',
        'nombreArticle',
    ];

    protected $casts = [
        'my_liste' => 'json',
];
}
