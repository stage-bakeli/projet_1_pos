<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'id_category',
        'nom',
        'id_fournisseur',
        'quantite',
        'description',
        'prix',
        'images'
    ];
}
