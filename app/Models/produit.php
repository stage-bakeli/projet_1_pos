<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_categories', 'nom', 'fornisseur', 'quantite', 'description',  'prix', 'nameImage'
    ];



    // protected function name(): Attribute
    // {
    //     return Attribute::make(
    //         get: fn ($value) => asset('produit/' . $value),
    //     );
    // }



    //     protected function name(): Attribute
    //     {
    //         return Attribute::make(
    //             get: fn ($value) => url('produit/'.$value),
    //         );
    //     }
}
