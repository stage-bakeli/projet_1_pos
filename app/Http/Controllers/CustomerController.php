<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Customer::all();
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'user_id' => ['required'],
            'nom' => ['required'],
            'email' => ['required'],
            'telephone' => ['required'],
            'adresse' => ['required'],
        ])->validate();

        Customer::create($request->all());
        return  response()->json([
            'message' => 'Client ajoutée avec succès!!'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return response()->json(['status' => 200, 'data' => $customer]);
    }



    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->nom = $request->nom;
        $customer->email = $request->email;
        $customer->telephone = $request->telephone;
        $customer->adresse = $request->adresse;

        if ($customer->save()) {
            return response()->json(["message" => "Mis à jour reussi"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if ($customer->delete()) {
            return response()->json(["message" => "Suppression reussi"]);
        }
    }
}
