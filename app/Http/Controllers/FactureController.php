<?php

namespace App\Http\Controllers;

use App\Models\Facture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FactureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Facture::all();
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'user_id' => ['required'],
            'my_liste' => ['required', 'array'],
            'id_client' => ['nullable'],
            'payer' => ['required'],
            'total' => ['required'],
            'nombreArticle' => ['required'],
        ])->validate();

        Facture::create($request->all());

        return response()->json([
            'message' => 'Facture validé avec succès!'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $facture = Facture::find($id);
        return response()->json(['status' => 200, 'data' => $facture]);
    }



    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $facture = Facture::find($id);
        $facture->my_liste = $request->my_liste;
        $facture->id_client = $request->id_client;
        $facture->payer = $request->payer;
        $facture->total = $request->total;
        $facture->nombreArticle = $request->nombreArticle;

        if ($facture->save()) {
            return response()->json(["message" => "Mis à jour reussi"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $facture = Facture::find($id);
        if ($facture->delete()) {
            return response()->json(["message" => "Suppression reussi"]);
        }
    }
}
