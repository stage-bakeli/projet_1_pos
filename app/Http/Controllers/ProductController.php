<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Product::all();
    }



    /**
     * Store a newly created resource in storage.
     */


    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'user_id' => ['required'],
            'id_category' => ['required'],
            'nom' => ['required'],
            'id_fournisseur' => ['required'],
            'quantite' => ['required'],
            'description' => ['required'],
            'prix' => ['required'],
            'image' => ['required', 'mimes:jpeg,png,jpg,gif,svg']
        ])->validate();

        $image_name = time() . '.' . $request->image->extension();
        $request->image->move(public_path('produit'), $image_name);

        $product = new Product([
            'user_id' => $request->user_id,
            'id_category' => $request->id_category,
            'nom' => $request->nom,
            'id_fournisseur' => $request->id_fournisseur,
            'quantite' => $request->quantite,
            'description' => $request->description,
            'prix' => $request->prix,
            'images' => $image_name
        ]);

        $product->save();

        return response()->json([
            'message' => 'Produit ajouté avec succès!'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response()->json(['status' => 200, 'data' => $product]);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        dd($request);
        $product = Product::find($id);
        // dd($product->fill($request->all()));

        if ($product) {

            // Valider les données
            $request->validate([
                'id_category' => 'required|numeric|min:0',
                'nom' => 'required',
                'id_fournisseur' => 'required',
                'quantite' => 'required|numeric|min:0',
                'description' => 'required|string',
                'prix' => 'required|numeric|min:0',
                // 'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            // Mettre à jour le product avec les nouvelles données
            $product->nom = $request->nom;
            $product->description = $request->description;
            $product->prix = $request->prix;
            $product->id_categories = $request->id_categories;
            $product->fornisseur = $request->fornisseur;
            $product->quantite = $request->quantite;
            $product->save();

            return 'sa fonction';
        }

        // Validator::make($request->all(), [
        //     'id_category' => ['required'],
        //     'nom' => ['required'],
        //     'id_fournisseur' => ['required'],
        //     'quantite' => ['required'],
        //     'description' => ['required'],
        //     'prix' => ['required'],
        //     'image' => 'mimes:jpeg,png,jpg,gif,svg'
        // ])->validate();

        // $product = Product::find($id);

        // if (!$product) {
        //     return response()->json([
        //         'message' => 'Produit non trouvé!'
        //     ], 404);
        // }

        // $product->update([
        //     'id_category' => $request->id_category,
        //     'nom' => $request->nom,
        //     'id_fournisseur' => $request->id_fournisseur,
        //     'quantite' => $request->quantite,
        //     'description' => $request->description,
        //     'prix' => $request->prix,
        // ]);

        // if ($request->hasFile('image')) {
        //     $image_name = time() . '.' . $request->image->extension();
        //     $request->image->move(public_path('produit'), $image_name);
        //     $product->update([
        //         'images' => $image_name
        //     ]);
        // }

        // return response()->json([
        //     'message' => 'Produit modifié avec succès!',
        //     'product' => $product
        // ]);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product->delete()) {
            return response()->json(['message' => 'Suppression reussi!']);
        }
    }
}
