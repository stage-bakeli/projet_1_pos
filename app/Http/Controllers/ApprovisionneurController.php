<?php

namespace App\Http\Controllers;

use App\Models\Approvisionneur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApprovisionneurController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Approvisionneur::all();
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'user_id' => ['required'],
            'nom' => ['required'],
            'email' => ['required'],
            'telephone' => ['required'],
            'adresse' => ['required'],
        ])->validate();

        Approvisionneur::create($request->all());
        return  response()->json([
            'message' => 'Fournisseur ajoutée avec succès!!'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show( $id)
    {
        $approvisionneur = Approvisionneur::find($id);
        return response()->json(['status' => 200, 'data' => $approvisionneur]);
    }

   
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,  $id)
    {
        $approvisionneur = Approvisionneur::find($id);
        $approvisionneur->nom = $request->nom;
        $approvisionneur->email = $request->email;
        $approvisionneur->telephone = $request->telephone;
        $approvisionneur->adresse = $request->adresse;

        if ($approvisionneur->save()) {
            return response()->json(["message" => "Mis à jour reussi"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy( $id)
    {
        $approvisionneur = Approvisionneur::find($id);
        if ($approvisionneur->delete()) {
            return response()->json(["message" => "Suppression reussi"]);
        }
    }
}
