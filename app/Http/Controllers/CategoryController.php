<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Category::all();
    }



    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'user_id' => ['required'],
            'nom' => ['required'],
            'description' => ['required'],
        ])->validate();

        Category::create($request->all());
        return  response()->json([
            'message' => 'Catégorie ajoutée avec succès!!'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show( $id)
    {
        $category = Category::find($id);
        return response()->json(['status' => 200, 'data' => $category]);   
    }

   

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $category= Category::find($id);
        $category->nom = $request->nom;
        $category->description = $request->description;

        if ($category->save()) {
            return response()->json(["message" => "Mis à jour reussi"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy( $id)
    {
        $category = Category::find($id);
        if ($category->delete()) {
            return response()->json(["message" => "Suppression reussi"]);
        }
    }
}
