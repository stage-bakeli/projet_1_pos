<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;
// use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProduitsController extends Controller

{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Produit::select('id', 'id_categories', 'nom', 'fornisseur', 'quantite', 'description',  'prix', 'nameImage', 'updated_at')->get();
    }


    /**
     * Store a newly created resource in storage.
     * 
     */



    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'id_categories' => ['required'],
            'nom' => ['required'],
            'fornisseur' => ['required'],
            'quantite' => ['required'],
            'description' => ['required'],
            'prix' => ['required'],
            'image' => 'mimes:jpeg,png,jpg,gif,svg'
        ])->validate();


        $image_name = time() . '.' . $request->image->extension();
        $request->image->move(public_path('produit'), $image_name);

        // dd($request->image);
        Produit::create([
            'id_categories' => $request->id_categories,
            'nom' => $request->nom,
            'fornisseur' => $request->fornisseur,
            'quantite' => $request->quantite,
            'description' => $request->description,
            'prix' => $request->prix,
            'nameImage' => $image_name
        ]);
        return  response()->json([
            'message' => 'Produit ajouté avec succès!!'
        ]);
    }



    /**
     * show the specified resource in storage.
     */

    public function show($id)
    {
        $categorie = Produit::find($id);
        return response()->json(['status' => 200, 'data' => $categorie]);
    }
    /**
     * Update the specified resource in storage.
     */


    // public function update(Request $request, $id)
    // {

    //     Validator::make($request->all(), [
    //         'id_categories' => ['required'],
    //         'nom' => ['required'],
    //         'fornisseur' => ['required'],
    //         'quantite' => ['required'],
    //         'description' => ['required'],
    //         'prix' => ['required'],
    //         'image' => 'mimes:jpeg,png,jpg,gif,svg'
    //     ])->validate();

    //     $produit = Produit::find($id);

    //     if (!$produit) {
    //         return response()->json(["status" => "Produit non trouvé"], 404);
    //     }

    //     if ($request->hasFile('image')) {

    //         // Supprimer l'ancienne image
    //         if ($produit->image) {
    //             $exists = Storage::disk('public')->exists("produit/{$produit->image}");
    //             if ($exists) {
    //                 Storage::disk('public')->delete("produit/{$produit->image}");
    //             }
    //         }

    //         // Télécharger la nouvelle image
    //         $image_name = time() . '.' . $request->image->extension();
    //         $request->image->move(public_path('produit'), $image_name);
    //         $produit->nameImage = $image_name;
    //     }

    //     // Mettre à jour les autres champs
    //     $produit->id_categories = $request->id_categories;
    //     $produit->nom = $request->nom;
    //     $produit->fornisseur = $request->fornisseur;
    //     $produit->quantite = $request->quantite;
    //     $produit->description = $request->description;
    //     $produit->prix = $request->prix;

    //     if ($produit->save()) {
    //         return response()->json(["status" => "Mise à jour réussie"]);
    //     } else {
    //         return response()->json(["status" => "Erreur lors de la mise à jour"], 500);
    //     }
    // }



    // public function update(Request $request, $id)
    // {
    //     $produit = Produit::find($id);
    //     // dd($request);

    //     // Validate request
    //     $validatedData = $request->validate([
    //         'id_categories' => 'required',
    //         'nom' => 'required',
    //         'fornisseur' => 'required',
    //         'quantite' => 'required',
    //         'description' => 'required',
    //         'prix' => 'required',
    //         'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //     ]);

    //     // Update produit with form data
    //     $produit->id_categories = $validatedData['id_categories'];
    //     $produit->nom = $validatedData['nom'];
    //     $produit->fornisseur = $validatedData['fornisseur'];
    //     $produit->quantite = $validatedData['quantite'];
    //     $produit->description = $validatedData['description'];
    //     $produit->prix = $validatedData['prix'];

    //     // If new image is provided, delete the old one and upload the new one
    //     if ($request->hasFile('image')) {
    //         // Delete the old image
    //         if ($produit->image) {
    //             Storage::delete('public/produit/' . $produit->image);
    //         }

    //         // Upload the new image
    //         $image = $request->file('image');
    //         $imageName = time() . '.' . $image->getClientOriginalExtension();
    //         $image->storeAs('public/produit', $imageName);

    //         // Update the produit with the new image name
    //         $produit->image = $imageName;
    //     }

    //     // Save the updated produit to the database
    //     $produit->save();

    //     if ($produit->save()) {
    //                 return response()->json(["status" => "Mise à jour réussie"]);
    //             } else {
    //                 return response()->json(["status" => "Erreur lors de la mise à jour"], 500);
    //             }
    // }


    public function update(Request $request, $id)
    {


        $produit = Produit::find(7);
        // dd($produit->fill($request->all()));

        if ($produit) {
            // Valider les données
            $request->validate([
                'nom' => 'required',
                'description' => 'required|string',
                'prix' => 'required|numeric|min:0',
                'id_categories' => 'required|numeric|min:0',
                'fornisseur' => 'required',
                'quantite' => 'required|numeric|min:0',
                // 'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            // Mettre à jour le produit avec les nouvelles données
            $produit->nom = $request->nom;
            $produit->description = $request->description;
            $produit->prix = $request->prix;
            $produit->id_categories = $request->id_categories;
            $produit->fornisseur = $request->fornisseur;
            $produit->quantite = $request->quantite;
            $produit->save();

            return 'sa fonction';
        }
        dd($produit);


        // if ($image = $request->file('image')) {
        //     $destinationPath = 'images/';
        //     $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        //     $image->move($destinationPath, $profileImage);
        //     $input['image'] = "$profileImage";
        // }else{
        //     unset($input['image']);
        // }

        // $product->update($input);

        // return redirect()->route('products.index')
        //                 ->with('success','Product has been updated successfully.');


        // $produit = Produit::find($id);

        // // Valider la requête
        // $validatedData = $request->validate([
        // 'id_categories' => 'required',
        // 'nom' => 'required',
        // 'fornisseur' => 'required',
        // 'quantite' => 'required|numeric|min:0',
        // 'description' => 'required',
        // 'prix' => 'required|numeric|min:0',
        // 'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        // ]);

        // // Mettre à jour le produit avec les données du formulaire
        // $produit->id_categories = $validatedData['id_categories'];
        // $produit->nom = $validatedData['nom'];
        // $produit->fornisseur = $validatedData['fornisseur'];
        // $produit->quantite = $validatedData['quantite'];
        // $produit->description = $validatedData['description'];
        // $produit->prix = $validatedData['prix'];

        // // Si une nouvelle image est fournie, supprimer l'ancienne et télécharger la nouvelle
        // if ($request->hasFile('image')) {
        //     // Supprimer l'ancienne image
        //     if ($produit->image) {
        //         Storage::delete('public/produit/' . $produit->image);
        //     }

        //     // Télécharger la nouvelle image
        //     $image = $request->file('image');
        //     $imageName = time() . '.' . $image->getClientOriginalExtension();
        //     $image->storeAs('public/produit', $imageName);

        //     // Mettre à jour le produit avec le nom de la nouvelle image
        //     $produit->image = $imageName;
        // }

        // // Enregistrer le produit mis à jour dans la base de données
        // $produit->save();

        // if ($produit->save()) {
        //     return response()->json(["status" => "Mise à jour réussie"]);
        // } else {
        //     return response()->json(["status" => "Erreur lors de la mise à jour"], 500);
        // }
    }

    /**
     * Remove the specified resource from storage.
     */


    public function destroy($id)
    {
        $produit = Produit::find($id);
        if ($produit->delete()) {
            return response()->json(["status" => "Suppression reussi"]);
        }
    }
}
