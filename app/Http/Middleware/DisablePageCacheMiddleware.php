<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DisablePageCacheMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        
        // Empêche la mise en cache de la page dans le navigateur
        $response->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        return $response;
    }
}
